package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.exceptions.InvalidMoveException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author pappgergely
 */
public class LabyrinthImpl implements Labyrinth {

    private int width = 5;
    private int height = 5;
    private int startCoordinate = 0;
    private CellType[][] labirinth = new CellType[width][height];

    public LabyrinthImpl() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                labirinth[i][j] = CellType.EMPTY;
            }
        }
    }

    @Override
    public void loadLabyrinthFile(String fileName) {
        try {
            Scanner sc = new Scanner(new File(fileName));
            int width = Integer.parseInt(sc.nextLine());
            int height = Integer.parseInt(sc.nextLine());

            for (int hh = 0; hh < height; hh++) {
                String line = sc.nextLine();
                for (int ww = 0; ww < width; ww++) {
                    switch (line.charAt(ww)) {
                        case 'W':

                            break;
                        case 'E':

                            break;
                        case 'S':

                            break;
                    }
                }
            }
        } catch (FileNotFoundException | NumberFormatException ex) {
            System.out.println(ex.toString());
        }
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public CellType getCellType(Coordinate c) throws CellException {
        if (c.getCol() < this.startCoordinate || c.getRow() < this.startCoordinate || c.getCol() > this.height || c.getRow() > this.width) {
            throw new CellException(c, "Nincs ilyen mező");
        }
        CellType answer = CellType.WALL;

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (labirinth[i][j].equals("W")) {
                    answer = CellType.WALL;
                } else if (labirinth[i][j].equals("E")) {
                    answer = CellType.END;
                } else if (labirinth[i][j].equals("S")) {
                    answer = CellType.START;
                } else {
                    answer = CellType.EMPTY;
                }
            }
        }
        return answer;
    }

    @Override
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public void setCellType(Coordinate c, CellType type) throws CellException {

        if (c.getCol() < this.startCoordinate || c.getRow() < this.startCoordinate || c.getCol() > this.height || c.getRow() > this.width) {
            throw new CellException(c, "Nincs ilyen mező");
        }
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (labirinth[i][j].equals(c.getCol()) && labirinth[i][j].equals(c.getRow())) {
                    labirinth[i][j] = type;
                }
            }
        }
    }

    @Override
    public Coordinate getPlayerPosition() {
        Coordinate cor = new Coordinate(0, 0);

        int i = 0;
        int j = 0;
        cor = new Coordinate(i, j);
        return cor;
    }

    @Override
    public boolean hasPlayerFinished() {

        boolean isPlayerWon = true;

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (labirinth[i][j].equals(CellType.END)) {
                    isPlayerWon = true;
                } else {
                    isPlayerWon = false;
                }
            }
        }
        return isPlayerWon;
    }

    @Override
    public List<Direction> possibleMoves() {

        ArrayList<Direction> directions = new ArrayList<>();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (labirinth[i][j] == CellType.EMPTY && labirinth[i][j + 1] == CellType.EMPTY) {
                    directions.add(Direction.SOUTH);
                }
                if (labirinth[i][j] == CellType.EMPTY && labirinth[i][j - 1] == CellType.EMPTY) {
                    directions.add(Direction.NORTH);
                }
                if (labirinth[i][j] == CellType.EMPTY && labirinth[i - 1][j] == CellType.EMPTY) {
                    directions.add(Direction.WEST);
                }
                if (labirinth[i][j] == CellType.EMPTY && labirinth[i + 1][j] == CellType.EMPTY) {
                    directions.add(Direction.EAST);
                }
            }
        }
        return directions;
    }

    @Override
    public void movePlayer(Direction direction) throws InvalidMoveException {

        Coordinate cor = new Coordinate(0, 0);
        int x = 0;
        int y = 0;
        cor = new Coordinate(x, y);

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (labirinth[i][j].equals(direction.EAST)) {
                    cor = new Coordinate(i + 1, j);
                } else if (labirinth[i][j].equals(direction.WEST)) {
                    cor = new Coordinate(i - 1, j);
                } else if (labirinth[i][j].equals(direction.NORTH)) {
                    cor = new Coordinate(i, j - 1);
                } else {
                    cor = new Coordinate(j, j+1);
                }
            }
        }
    }
}
