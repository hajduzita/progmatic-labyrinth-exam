/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.labyrinthproject.interfaces;

import com.progmatic.labyrinthproject.enums.Direction;

/**
 *
 * @author kralalabe
 */
public class RandomPlayer implements Player {

//    private int i;
//    private int j;
//    private int[][] position;

    @Override
    public Direction nextMove(Labyrinth l) {
        Direction d = Direction.EAST;

        for (int i = 0; i < l.getWidth(); i++) {
            for (int k = 0; k < l.getHeight(); k++) {

                int random = (int) (Math.random() * 3);

                if (random == 0) {
                    d = Direction.EAST;
                } else if (random == 1) {
                    d = Direction.NORTH;
                } else if (random == 2) {
                    d = Direction.SOUTH;
                } else {
                    d = Direction.WEST;
                }
                return d;
            }

        }
