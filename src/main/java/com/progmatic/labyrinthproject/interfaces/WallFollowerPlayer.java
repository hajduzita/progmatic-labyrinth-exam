/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.labyrinthproject.interfaces;

import com.progmatic.labyrinthproject.enums.Direction;

/**
 *
 * @author kralalabe
 */
public class WallFollowerPlayer implements Player {

    @Override
    public Direction nextMove(Labyrinth l) {
        return Direction.WEST;
    }

}
